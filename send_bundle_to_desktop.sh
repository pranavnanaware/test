#!/bin/bash

flutter clean && flutter build appbundle
if [[ $? == 0 ]]; then
    echo "COPYING BUNDLE...."
    cp -f $PWD/build/app/outputs/bundle/release/app-release.aab $HOME/Desktop/
    echo "BUNDLE COPIED."
    echo "RENAMING BUNDLE...."
    mv $HOME/Desktop/app-release.aab $HOME/Desktop/TLA-BUNDLE-$(date +%F-%H:%M).aab
else
    echo "FLUTTER BUNDLE BUILD FAILED"
fi
